from cachorro import cachorro

v = cachorro()

v.tamanho = 3.4
v.peso = 3.9
v.cor = "Azul"

print("""
	O tamanho do cachorro é {}
	O peso do cachorro é {}
	A cor do cachorro é {}
	""".format(v.tamanho, v.peso, v.cor))

print(vars(v))
