# Faça um programa que receba uma quantidade de produtos, e o desconto a ser aplicado em cada produto, e assim, no final, printa o valor que pagaria, e printa o novo valor com desconto a ser pago.

def askNumber():
	# setando duas variaveis
	totalSD = 0
	totalCD = 0

	# abrindo um while para ser possível adicionar quantos produtos o usuário desejar
	while True:
		# o try é usado para erros
		try:
			# pergunta para o usuário se quer adicionar um novo produto
			newProd =  int(input("Digite 1 se deseja inserir algo, ou 2 se deseja finalizar: "))
			# caso não, ele sai do while
			if newProd == 2:
				break
			# caso não seja um valor válido, chama ("raise") o erro "TypeError"
			if newProd != 1:
				raise TypeError
			# pergunta o valor do produto
			vp = int(input("Informe o valor: "))
			# pergunta o valor do desconto
			vd = int(input("Informe a porcentagem de desconto ( 0-100 ) : "))
			# caso os valores não sejam positivos, chama ("raise") o erro "NameError"
			if vp < 0 or vd < 0:
				raise NameError
			# cria variáveis para armazenar os totais com e sem desconto
			totalSD += vp
			totalCD += vp*((100 - vd)/100)
		# caso seja chamado o "Name Error", printa
		except NameError:
			print("Digite somente números positivos.\n\n")
		# caso seja chamado o "TypeError", printa
		except TypeError:
			print("Digite somente 1 ou 2.\n\n")
		# caso seja chamado o "ValueError", printa
		except ValueError:
			print("Digite somente números.\n\n")
	# retorna os totais
	return totalSD, totalCD
# func main
	# cria duas variáveis para receberem os valores que serão retornados na função chamada
valor, valorFinal = askNumber()
	# printa os totais e finaliza o programa
print("Você pagaria: ", valor,"\n\n Você pagará; ", valorFinal)
