// No hotel morte lenta, estavam hospedados 5 hospédes, Renato Faca, Maria Cálice, Roberto Arma, Roberta Corda e Uesllei.
// A meia noite, o hóspede do quarto 101 foi encontrado morto e os 5 suspeitos são os citados acima.
// Descubra quem é o assassino sabendo que:
//        Se o nome do suspeito contem 3 vogais ou mais(Nome completo) e nenhuma dessas vogais é u ou o, ele pode ser o assassino.
//        Se o suspeito for do sexo feminino, só sera assassina se tiver utilizado uma arma branca.
//        Se o suspeito for do sexo masculino, ele deveria estar no saguão do hotel a 00h30 para ser considerado o assasino.
// Sabendo dessas informações acima, projete o algorítmo que fornecido o nome do suspeito, indique se ele pode ou não ser o assassino.

package main

import ("fmt"
		"strings")

func main() {
	// solcitar nome ao usuário
	name := askString("Nome: ")
		// verifica se o nome é uma string
	if !validString(name, "1234567890!@#$%*()<>.,:;?/"){
		return 
	}

	// solcitar nome ao usuário
	sobre := askString("Sobrenome: ")
		//verifica se o sobrenome é uma string
	if !validString(sobre, "1234567890!@#$%*()<>.,:;?/"){
		return
	}

	// solcitar genero ao user
	gender := askString("Selecione seu gênero - F/M/O - :  ")
		//verifica se o gênero é valido
	if !validString(gender, "abcdegHijklnpqrstuvwxyzABCDEGHIJKLNPQRSTUVWXYZ1234567890!@#$%*()<>.,:;?/"){
		return
	}

	// solicitar hora ao user
	time := askString("Informe a hora no seguinte formato - 00h00 - :  ")
		//verifica o formato do horário
	if !validString(time, "abcdefgijklmnopqrstuvwxyzABCDEFGIJKLMNOPQRSTUVWXYZ!@#$%*()<>.,:;?/"){
		return
	}

	// se tudo for válido, printa que o programa vai começar a investigar o suspeito.
	fmt.Println("Verificando se o ", name, " ", sobre, ", do gênero biológico", gender, ", é inocente ou possível assassino.")
	
	//caso1 - Se o nome do suspeito contem 3 vogais ou mais(Nome completo) e nenhuma dessas vogais é u ou o, ele pode ser o assassino.
	caseOne := fristCase(name)
	if !caseOne{
		fmt.Println("Inocente.")
		return
	}

	//caso2 - Se o suspeito for do sexo feminino, só sera assassina se tiver utilizado uma arma branca.
	var caseTwo bool
	if !checkString(gender, "F"){
		caseTwo = secondCase(sobre)
	}
	if !caseTwo{
		fmt.Println("Inocente.")
		return
	}
	
	//caso3	- Se o suspeito for do sexo masculino, ele deveria estar no saguão do hotel a 00h30 para ser considerado o assasino.
	var caseThree bool
	if !checkString(gender, "M"){
		caseThree = lastCase(time)
	}
	if !caseThree{
		fmt.Println("Inocente.")
		return
	}

	fmt.Println("Possível assasino(a).")

}

func askString(msg string) string {

	// declara a var input
	var input string

	// apresenta a string Nome on-the-face
	fmt.Print(msg)
    
    // lê o que o usuário digitou
    fmt.Scanln(&input)

    return input
}

func checkString(s string, chars string) bool {
	return !strings.ContainsAny(s, chars)
}

func validString(msg string, rule string) bool{
	
	// verifica se a msg é uma string
	vld := checkString(msg, rule)
	if !vld{
		fmt.Println("Digita direito cacete.")
		return false
	}
	return true
}

func fristCase(name string) bool{
	if !checkString(name, "ouOU"){
		return false
	}
	cntr := 0
	for vvowel, r := range name {
		if ((vvowel == 'a') || (vvowel == 'e') || (vvowel == 'i')){
			cntr = cntr + 1
			//just ignore this
			r = r
		}
		if cntr >= 3 {
			return true
		}
	}
	return false
}

func secondCase(sobre string) bool{
	//if !checkString(sobre, "MO"){
	//	return false
	//}
	if ((sobre == "Calice") || (sobre == "Corda")){
		return true
	}
	return false
}

func lastCase(time string) bool{
	if (time == "00h30"){
		return true
	}
	return false
}
