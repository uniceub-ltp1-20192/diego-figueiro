package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	// cria uma variável para receber o primeiro valor
	number_one := askNumber("Digite primeiro nº: ")
	// cria uma variável para receber a operação a ser realizada
	operation  := askOperation("Operação: ") 
	// cria uma variável para receber o segundo valor
	number_two := askNumber("Digite segundo nº: ")
	// cria uma variável para receber o valor do resultado
	result := calculate(number_one, number_two, operation)
	// mostra o valor on-the-face
	printResult(result)
}

func askNumber(msg string) float64 {
	while := 0
	for while < 1 {
		// declara a var input
		var input string
		// apresenta a string Nome on-the-face
		fmt.Print(msg)
	    // lê o que o usuário digitou
	    fmt.Scanln(&input)
	    valor, erro := strconv.ParseFloat(input, 64)
	    if erro != nil {
	    	fmt.Println(erro)
			fmt.Println("Somente números são permitidos.")
			continue
		} 
		while++
		return valor
	}
    return 0
}


func askOperation(msg string) string {
	// declara a var input
	var op string

	// apresenta a string Nome on-the-face
	
    while := 0
	for while < 1 {
		fmt.Print(msg)
    
    	// lê o que o usuário digitou
    	fmt.Scanln(&op)
		if !strings.ContainsAny(op, "+-*/"){
			print("Escolha uma operação válida. ex: * + - /\n")
			continue
		}
		if len(op) > 1{
			print("Digite apenas uma operação por vez.\n")
			continue
		}
		while++
	}


    return op
}

func calculate(n float64, m float64, op string) float64 {
	
	if (op == "+") {
		return n + m
	}

	if (op == "-") {
		return n - m
	}

	if (op == "*") {
		return n * m
	}

	return n / m
}

func printResult(number interface{}) {

	fmt.Printf("O resultado é: %v \n", number)
}
