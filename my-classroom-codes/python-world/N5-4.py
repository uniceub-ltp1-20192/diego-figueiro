""" Crie uma programa que fornecidos a idade e o semestre
que um pessoa está informa em quantos anos ela vai se
formar."""
def askNumber(msg):
	num = int(input(msg))
	return num
def ageForm(age, sem):
	max_sem = 12
	sem_age =  (12 - sem)/2
	fage = age + sem_age
	return fage

age = askNumber("Insira sua idade:  ")
sem = askNumber("Insira seu semestre:  ")

result = ageForm(age, sem)
print("Vc levará até seus "+str(result)+" anos para se formar.")
