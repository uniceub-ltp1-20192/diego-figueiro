"""Use o terceiro parâmetro de função range para fazer
uma lista com os números impares de 1 a 20. Imprima cada
elemento da lista separadamente."""

def main():
	lista = createNumList()
	printList(lista)

def createNumList():
	# Criando uma lista vasia.
	lista = []
	# loop para o menu
	lista = [nums for nums in range(int(input("Digite o número de início da lista: ")),
		(int(input("Digite o número de finalização da lista: "))+1),
		int(input("Digite o passo da lista: ")))]
	return lista

def printList(lista):
	for i in lista:
		print(i)

main()