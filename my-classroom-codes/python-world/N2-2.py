''' Faça um comentário em português em cada linha do programa
abaixo explicando para você mesmo que elas fazem. Execute o
programa abaixo, corrigindo os erros. '''

# declarando a variável
cebolas = 300

# declarando a variável
cebolas_na_caixa = 120

# declarando a variável
espaco_caixa = 5

# declarando a variável
caixas = 60

# declarando a variável e igualando-a uma operação entre variáveis
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa

# declarando a variável e igualando-a uma operação entre variáveis
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)

# declarando a variável e igualando-a uma operação entre variáveis
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa

# imprimindo para o usuário final
print ("Existem ", cebolas_na_caixa, " cebolas encaixotadas")

# imprimindo para o usuário final
print ("Existem ", cebolas_fora_da_caixa, " cebolas sem caixa")

# imprimindo para o usuário final
print ("Em cada caixa cabem ", espaco_caixa, " cebolas")

# imprimindo para o usuário final
print ("Ainda temos, ", caixas_vazias, " caixas vazias")

# imprimindo para o usuário final
print ("Então, precisamos de ", caixas_necessarias, " caixas para empacotar todas as cebolas")