"""Acabou de ouvir que um dos seus convidados não pode
fazer o jantar, por isso precisa de enviar um novo
conjunto de convites. Você terá que pensar em outra
pessoa para convidar.
• Comece com o seu programa do exercício 7.1. Adicione
uma declaração de impressão no final do programa,
informando o nome do convidado que não pode
comparecer.
• Modifique sua lista, substituindo o nome do
convidado que não pode comparecer com o nome da nova
pessoa que você está convidando.

• Imprima um segundo conjunto de mensagens de convite,
uma para cada pessoa que ainda esteja na sua lista."""

def main():
	# Criando a lista de pessoas
	pessoas = createList()
	# Convidando as pessoas da lista
	inviteList(pessoas, ", convido-te para jantar.")
	# alguém não pode comparecer
	print((pessoas[0].title())+" não pode comparecer.\n Convide outra pessoa.")
	# removendo uma pessoa e adicionando outra
	editList(pessoas)
	# convidando pessoas que ainda estão na lista
	inviteList(pessoas, ", convido-te para jantar.")

def createList():
	# Criando uma lista vasia.
	lista = []
	# loop para o menu
	key = True
	while key == True:
		# Menu para perguntar ação ao usuário
		action = input("Digite a tecla correspondente a ação desejada: \n "+
			"\t1 - Para adicionar convidado.\n"+
			"\t2 - Para finalizar lista.\n")
		# se for finalizar, entra no if
		if action == "2":
			# torna a key falsa, finalizando o loop
			key = False
			continue
		# se for adicionar um novo elemento, entra no if
		if action == "1":
			# adicionando novo elemento ao final da lista
			lista.append(input("Informe o nome do convidado(a):  "))
			# retornando ao menu
			continue
	# retorna a lista completa
	return lista

def editList(lista):
	key = True
	while key == True:
		# Menu para perguntar ação ao usuário
		action = input("Digite a tecla correspondente a ação desejada: \n "+
			"\t1 - Para remover convidado.\n"+
			"\t2 - Para adicionar convidado.\n"+
			"\t3 - Para finalizar alterações.\n")
		# se for finalizar, entra no if
		if action == "1":
			lista.remove(input("Nome do convidado a ser removido: "))
			continue
		if action == "2":
			lista.append(input("Nome do convidado a ser adicionado: "))
			continue
		if action == "3":
			key = False
			continue
	return lista

# função que recebe uma lista e uma string a ser printada para cada elemento
def inviteList(lista, msg):
	for i in range(len(lista)):
		print(lista[i].title()+msg)
# executa o main
main()






















"""pessoas = ['cebola', 'batato', 'repolho', 'tomata', 'mirtila', 'uesllei']

for i in range(len(pessoas)):
	print("Caro(a) "+pessoas[i].title()+" convido-te para jantar.")

faltou = pessoas.pop(-1)
print("\t\n"+faltou.title()+" não pode comparecer.\n")

pessoas.insert(-1, 'cenoro')

for i in range(len(pessoas)):
	print("Caro(a) "+pessoas[i].title()+" convido-te para jantar.")"""