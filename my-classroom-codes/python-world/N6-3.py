""" Pense no seu modo de transporte favorito, como uma
moto ou um carro, e faça uma lista que armazene modelos
do seu transporte favorito. Use sua lista para imprimir
uma série de mensagens sobre esses itens, como "Eu
gostaria de ter uma moto da Honda". """

máquina_agrícola = ['valtra', 'massey', 'deere', 'jacto', 'baldan']
jet_ski = ['sentado', 'yamaha', 'kawasaki', 'sea doo', 'belassi']

for i in range(len(máquina_agrícola)):
	print("Eu gostaría de ter um maquinário agrícolo da "+máquina_agrícola[i].title())

for i in range(len(jet_ski)):
	print("Adoraria surfar num jet ski da "+jet_ski[i].title())
	