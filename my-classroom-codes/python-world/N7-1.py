"""Se você pudesse convidar alguém, vivo ou morto, para
jantar, quem você convidaria? Faça uma lista que inclua
pelo menos três pessoas que você gostaria de convidar
para o jantar. Em seguida, use sua lista para imprimir
uma mensagem para cada pessoa, convidando­a para jantar."""

def main():
	# Criando a lista de pessoas
	pessoas = createList()
	# Convidando as pessoas da lista
	inviteList(pessoas, ", convido-te para jantar.")

def createList():
	# Criando uma lista vasia.
	lista = []
	# loop para o menu
	key = True
	while key == True:
		# Menu para perguntar ação ao usuário
		action = input("Digite a tecla correspondente a ação desejada: \n "+
			"\t1 - Para adicionar convidado.\n"+
			"\t2 - Para finalizar lista.\n")
		# se for finalizar, entra no if
		if action == "2":
			# torna a key falsa, finalizando o loop
			key = False
			continue
		# se for adicionar um novo elemento, entra no if
		if action == "1":
			# adicionando novo elemento ao final da lista
			lista.append(input("Informe o nome do convidado(a):  "))
			# retornando ao menu
			continue
	# retorna a lista completa
	return lista
# função que recebe uma lista e uma string a ser printada para cada elemento
def inviteList(lista, msg):
	for i in range(len(lista)):
		print(lista[i].title()+msg)
# executa o main
main()
