""" Conceba uma programa que informe se um determinado
número é primo. """

from math import sqrt
from itertools import count, islice

def verfPrimo(num):
    if num < 2:
        return False

    for number in islice(count(2), int(sqrt(num) - 1)):
        if num % number == 0:
            return False

    return True
number = int(input("Digite um valor: "))
if verfPrimo(number):
	print("é primo")
else:
	print("não primo")
