"""Faça uma lista de de 1 a um milhão e use as funções
min() e max() para grantir que sua lista realmente começa
em 1 e termina em um milhão. Depois utilize a função
sum() para imprimir a soma dos valores da lista."""

def main():
	lista = createNumList()
	minMaxSumList(lista)

def createNumList():
	# Criando uma lista vasia.
	lista = []
	# loop para o menu
	lista = [nums for nums in range(int(input("Digite o número de início da lista: ")),
		(int(input("Digite o número de finalização da lista: "))+1),
		int(input("Digite o passo da lista: ")))]
	return lista

def minMaxSumList(lista):
	print("Menor numero: ", str(min(lista)))
	print("Maior numero: ", str(max(lista)))
	print("Soma de todos: ", str(sum(lista)))

main()