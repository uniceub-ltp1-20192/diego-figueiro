"""Faça uma lista com todos os números múltiplos de 3,
no intervalo de 3 a 1000. Imprima cada valor
separadamente."""

def main():
	lista = createNumList()
	printList(lista)

def createNumList():
	# Criando uma lista vasia.
	lista = []
	# loop para o menu
	lista = [nums for nums in range(int(input("Digite o número de início da lista: ")),
		(int(input("Digite o número de finalização da lista: "))+1),
		int(input("Digite o passo da lista: ")))]
	return lista

def printList(lista):
	for i in lista:
		print(i)

main()