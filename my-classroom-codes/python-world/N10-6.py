"""Um número elevado a terceira potência e chamado de
cubo. Faça uma lista com todos os cubos entre 1 e 100,
imprima cada valor separadamente"""

def main():
	lista = createNumList()
	cuboList(lista)

def createNumList():
	# Criando uma lista vasia.
	lista = []
	# loop para o menu
	lista = [nums for nums in range(int(input("Digite o número de início da lista: ")),
		(int(input("Digite o número de finalização da lista: "))+1),
		int(input("Digite o passo da lista: ")))]
	return lista

def printList(lista):
	for i in lista:
		print(i)

def cuboList(lista):
	cubos = []
	for i in lista:
		r = i**3
		cubos.append(r)
	printList(cubos)

main()