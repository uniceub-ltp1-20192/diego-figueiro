"""Faça compreensão de lista para gerar a lista do
exercício 10.6"""

def main():
	lista = createNumList()
	cuboList(lista)

def createNumList():
	# Criando uma lista vasia.
	lista = []
	# loop para o menu
	lista = [nums for nums in range(int(input("Digite o número de início da lista: ")),
		(int(input("Digite o número de finalização da lista: "))+1),
		int(input("Digite o passo da lista: ")))]
	return lista

def printList(lista):
	for i in lista:
		print(i)

def cuboList(lista):
	cubos = []
	for i in lista:
		cubos.append(i**3)
	printList(cubos)

main()