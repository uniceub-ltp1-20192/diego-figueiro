"""Pense em algo que você gostaria
armazenar em uma lista. Por exemplo, você pode criar uma
lista de montanhas, rios,
países, cidades, idiomas ou
qualquer outra coisa que
deseje. Escreva um programa
que crie uma lista contendo
esses itens e, em seguida,
use cada função apresentada
nesta nota pelo menos uma
vez."""

def main():
	lista = createList()
	allFunctions(lista)

def createList():
	# Criando uma lista vasia.
	lista = []
	# loop para o menu
	key = True
	while key == True:
		# Menu para perguntar ação ao usuário
		action = input("\nDigite a tecla correspondente a ação desejada: \n "+
			"\t1 - Para adicionar elemento.\n"+
			"\t2 - Para finalizar lista.\n")
		# se for finalizar, entra no if
		if action == "2":
			# torna a key falsa, finalizando o loop
			key = False
			continue
		# se for adicionar um novo elemento, entra no if
		if action == "1":
			# adicionando novo elemento ao final da lista
			lista.append(input("Informe o elemento:  "))
			# retornando ao menu
			continue
	# retorna a lista completa
	return lista

def allFunctions(lista):
	print(lista)
	print(len(lista))
	print(sorted(lista))
	print(lista)
	lista.reverse()
	print(lista)
	lista.reverse()
	print(lista)
	lista.sort()
	print(lista)
	lista.sort(reverse=True)
	print(lista)

main()