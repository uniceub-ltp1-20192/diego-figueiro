"""Implemente um programa que fornecida uma lista de
palavras e uma palavra indique quais elementos da lista
são anagramas da palavra."""

def main():
	lista = createList()
	key = input("Digite uma palavra: ")
	print(anagramCheck(key, lista))

def createList():
	# Criando uma lista vasia.
	lista = []
	# loop para o menu
	key = True
	while key == True:
		# Menu para perguntar ação ao usuário
		action = input("\nDigite a tecla correspondente a ação desejada: \n "+
			"\t1 - Para adicionar elemento.\n"+
			"\t2 - Para finalizar lista.\n")
		# se for finalizar, entra no if
		if action == "2":
			# torna a key falsa, finalizando o loop
			key = False
			continue
		# se for adicionar um novo elemento, entra no if
		if action == "1":
			# adicionando novo elemento ao final da lista
			lista.append(input("Informe o elemento:  "))
			# retornando ao menu
			continue
	# retorna a lista completa
	return lista

def anagramCheck(key, lista):
	result = []
	for i in lista:
		if sorted(i) == sorted(key):
			result.append(i)
	return result

main()