"""Quero loops, mais loops! Todos os códigos nessa nota de aula
imprimiram as listas sem o uso de loops. Escolha um código dessa
nota e imprima todos os itens como um cardápio. Use um loop apra
cada lista."""


players = ['ceboleto', 'tomatino', 'pepinera', 'brocolin', 'nabo']
print("Os nomes dos três primeiros jogadores:")
for player in players[:3]:
	print(player.title())