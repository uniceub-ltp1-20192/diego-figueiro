"""Implemente um código que imprima uma lista com o
tamanho de cada elemento da lista de entrada."""

def main():
	lista = createList()
	lenOfElements(lista)

def createList():
	# Criando uma lista vasia.
	lista = []
	# loop para o menu
	key = True
	while key == True:
		# Menu para perguntar ação ao usuário
		action = input("\nDigite a tecla correspondente a ação desejada: \n "+
			"\t1 - Para adicionar elemento.\n"+
			"\t2 - Para finalizar lista.\n")
		# se for finalizar, entra no if
		if action == "2":
			# torna a key falsa, finalizando o loop
			key = False
			continue
		# se for adicionar um novo elemento, entra no if
		if action == "1":
			# adicionando novo elemento ao final da lista
			lista.append(input("Informe o elemento:  "))
			# retornando ao menu
			continue
	# retorna a lista completa
	return lista

def lenOfElements(lista):
	for i in lista:
		print(len(i))
	
main()