"""Pense em pelo menos cinco lugares no mundo que você
gostaria de visitar.

• Armazene os locais em uma lista. Certifique­se
de que a lista não esteja em ordem alfabética.

• Imprima sua lista na ordem original. Não se
preocupe em imprimir a lista item por item,
basta imprimi­la como uma lista bruta do Python.
• Use sorted() para imprimir sua lista em ordem
alfabética sem modificar a lista atual.
• Mostre que sua lista ainda está em sua ordem
original, imprimindo­a.
• Use sorted() para imprimir sua lista em ordem
alfabética inversa sem alterar a ordem da lista
original.
• Mostre que sua lista ainda está em sua ordem
original, imprimindo­a.
• Use reverse() para alterar a ordem da sua lista.
Imprima a lista para mostrar que a ordem foi
alterada.
• Use reverse() para alterar a ordem da sua lista
novamente. Imprima a lista para mostrar que ela
está de volta ao pedido original.
• Use sort() para que a lista seja armazenada em
ordem alfabética. Imprima a lista para mostrar
que a ordem foi alterada.
• Use sort() para que a lista seja armazenada em
ordem alfabética inversa. Imprima a lista para
mostrar que a ordem foi alterada."""

def main():
	locations = createList()
	print(locations)

	print(sorted(locations))
	print(locations)

	locations.reverse()
	print(locations)

	locations.reverse()
	print(locations)

	locations.sort()
	print(locations)

	locations.sort(reverse=True)
	print(locations)

def createList():
	# Criando uma lista vasia.
	lista = []
	# loop para o menu
	key = True
	while key == True:
		# Menu para perguntar ação ao usuário
		action = input("\nDigite a tecla correspondente a ação desejada: \n "+
			"\t1 - Para adicionar item.\n"+
			"\t2 - Para finalizar lista.\n")
		# se for finalizar, entra no if
		if action == "2":
			# torna a key falsa, finalizando o loop
			key = False
			continue
		# se for adicionar um novo elemento, entra no if
		if action == "1":
			# adicionando novo elemento ao final da lista
			lista.append(input("Informe o item:  "))
			# retornando ao menu
			continue
	# retorna a lista completa
	return lista

main()