"""Armazene o nome de uma pessoa e inclua alguns
caracteres de espaço em branco no início e no final do
nome. Certifique­se de usar cada combinação de
caracteres, "t" e "n", pelo menos uma vez. Imprima o
nome uma vez, para que o espaço em branco ao redor do
nome seja exibido. Em seguida, imprima o nome usando cada
uma das três funções de DECAPAGEM (stripping) lstrip(),
rstrip() e strip()."""

name = " cebola "
print("\t"+name)
print(name.lstrip())
print(name.rstrip())
print("\n"+name.strip())
