"""Implemente um código em Python que multiplique todos
elementos dentro de uma lista."""


def main():
	nums = createNumList()
	print(nums)
	print(times(nums))

def createNumList():
	# Criando uma lista vasia.
	lista = []
	# loop para o menu
	lista = [nums for nums in range(int(input("Digite o número de início da lista: ")),
		(int(input("Digite o número de finalização da lista: "))+1),
		int(input("Digite o passo da lista: ")))]
	return lista

def times(lista):
	result = 1
	for i in lista:
		result = result * i
	return result

main()
