"""Você acabou de encontrar uma mesa de jantar maior,
então agora há mais espaço disponível. Pense em mais três
convidados para convidar para o jantar.
• Comece com o seu programa no Exercício 7.2. Adicione
uma mensagem de impressão ao final do programa
informando às pessoas que você encontrou uma mesa de
jantar maior
• Use insert() para adicionar um novo convidado ao
início de sua lista.
• Use insert () para adicionar um novo convidado ao
meio da sua lista.
• Use append() para adicionar um novo convidado ao
final da sua lista.
• Imprima um novo conjunto de mensagens de convite,
uma para cada pessoa na sua lista."""

def main():
	# Criando a lista de pessoas
	pessoas = createList()
	# Convidando as pessoas da lista
	inviteList(pessoas, ", convido-te para jantar.")
	# alguém não pode comparecer
	print((pessoas[0].title())+" não pode comparecer.\n Convide outra pessoa.")
	# removendo uma pessoa e adicionando outra
	editList(pessoas)
	# convidando pessoas que ainda estão na lista
	inviteList(pessoas, ", convido-te para jantar.")
	# convide mais pessoas por outros métodos
	print("Encontrei uma mesa maior, vou convidar mais pessoas!")
	# insira uma pessoa no início da lista, outra no meio e outra no final
	insertElement(pessoas)
	# convide quem está na lista
	inviteList(pessoas, ", convido-te para jantar.")

def insertElement(lista):
	key = True
	while key == True:
		# Menu para perguntar ação ao usuário
		action = input("\nDigite a tecla correspondente a ação desejada: \n "+
			"\t1 - Adicionar convidado no início da lista.\n"+
			"\t2 - Adicionar convidado no meio da lista.\n"+
			"\t3 - Adicionar convidado no final da lista.\n"+
			"\t4 - Finalizar alterações.\n")
		# se for finalizar, entra no if
		if action == "1":
			lista.insert(0,(input("Nome do convidado a ser adicionado no início da lista: ")))
			continue
		if action == "2":
			lista.insert((int(((len(lista))+1)/2)),(input("Nome do convidado a ser adicionado no meio da lista: ")))
		if action == "3":
			lista.append(input("Nome do convidado a ser adicionado no final da lista: "))
			continue
		if action == "4":
			key = False
			continue
	return lista

def createList():
	# Criando uma lista vasia.
	lista = []
	# loop para o menu
	key = True
	while key == True:
		# Menu para perguntar ação ao usuário
		action = input("\nDigite a tecla correspondente a ação desejada: \n "+
			"\t1 - Para adicionar convidado.\n"+
			"\t2 - Para finalizar lista.\n")
		# se for finalizar, entra no if
		if action == "2":
			# torna a key falsa, finalizando o loop
			key = False
			continue
		# se for adicionar um novo elemento, entra no if
		if action == "1":
			# adicionando novo elemento ao final da lista
			lista.append(input("Informe o nome do convidado(a):  "))
			# retornando ao menu
			continue
	# retorna a lista completa
	return lista

def editList(lista):
	key = True
	while key == True:
		# Menu para perguntar ação ao usuário
		action = input("\nDigite a tecla correspondente a ação desejada: \n "+
			"\t1 - Para remover convidado.\n"+
			"\t2 - Para adicionar convidado.\n"+
			"\t3 - Para finalizar alterações.\n")
		# se for finalizar, entra no if
		if action == "1":
			lista.remove(input("Nome do convidado a ser removido: "))
			continue
		if action == "2":
			lista.append(input("Nome do convidado a ser adicionado: "))
			continue
		if action == "3":
			key = False
			continue
	return lista



# função que recebe uma lista e uma string a ser printada para cada elemento
def inviteList(lista, msg):
	for i in range(len(lista)):
		print(lista[i].title()+msg)
# executa o main
main()

