"""Crie uma variação do programa 5.4 que receba também a
informação de quantos semestres o aluno está atrasado e
indique qual o tempo total que ele vai ter no curso até
se formar."""

def askNumber(msg):
	num = int(input(msg))
	return num
def ageForm(age, sem, atr):
	max_sem = 12
	max_w_atr_sem = 12 + atr
	tempo = ((max_w_atr_sem - sem)/2)+age
	return tempo

age = askNumber("Insira sua idade:  ")
sem = askNumber("Insira seu semestre:  ")
atr = askNumber("Insira quantos semestres vc está atrasado: ")

result = ageForm(age, sem, atr)
print("Vc levará até seus "+str(result)+" anos para sua formação.")
