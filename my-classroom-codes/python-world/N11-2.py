"""Crie uma lista de sabores de pizza e armazene em pizza_hut.
Faça uma cópia da lista de pizzas e armazene em
pizza_hot_paraguai. E faça o que se pede:
• Adiciona uma pizza em pizza_hut.
• Adicione uma pizza diferente em pizza_hot_paraguai.
• Demonstre que a pizza hut original é diferente da
cópia paraguaia!"""

def main():
	pizza_hut = createList()
	pizza_hot_paraguai = pizza_hut[:]
	pizza_hut.append("80 queijos")
	pizza_hot_paraguai.append("9 presuntos")
	print(pizza_hut, "\n\n", pizza_hot_paraguai)


def createList():
	# Criando uma lista vasia.
	lista = []
	# loop para o menu
	key = True
	while key == True:
		# Menu para perguntar ação ao usuário
		action = input("\nDigite a tecla correspondente a ação desejada: \n "+
			"\t1 - Para adicionar elemento.\n"+
			"\t2 - Para finalizar lista.\n")
		# se for finalizar, entra no if
		if action == "2":
			# torna a key falsa, finalizando o loop
			key = False
			continue
		# se for adicionar um novo elemento, entra no if
		if action == "1":
			# adicionando novo elemento ao final da lista
			lista.append(input("Informe o elemento:  "))
			# retornando ao menu
			continue
	# retorna a lista completa
	return lista

main()