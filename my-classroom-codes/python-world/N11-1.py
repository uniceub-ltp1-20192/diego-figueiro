"""Escolha algum dos programas que você fez sobre
listas, e adicione algumas linhas ao final do programa para que
ele faça o seguinte:

• Imprima a seguinte mensagem: “Os primeiros 3 itens da
lista são: ”. E use um slice para imprimir os três
primeiros itens da lista do programa escolhido.
• Imprima a seguinte mensagem: “Os últimos 3 itens da
lista são: ”. E use um slice para imprimir os três
últimos itens da lista do programa escolhido.
• Imprima a seguinte mensagem: “Os 3 itens no meio da
lista são: ”. E use um slice para imprimir os três itens
no meio da lista do programa escolhido."""


def main():
	lista = ['a', 'b', 'c', 'd', 'e', 'f', 'h']
	printSlices(lista)

def printSlices(lista):
	print(lista[:3])
	print(lista[2:5])
	print(lista[4:])

main()
