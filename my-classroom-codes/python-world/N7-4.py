"""Você acabou de descobrir que sua nova mesa de jantar
não chegará a tempo para o jantar e terá espaço para
apenas dois convidados.
• Comece com seu programa do Exercício 7.3. Adicione
uma nova linha que imprima uma mensagem dizendo que
você pode convidar apenas duas pessoas para o
jantar.
• Use pop() para remover os convidados da sua lista,
um de cada vez, até que apenas dois nomes permaneçam
na sua lista. Sempre que você inserir um nome na sua
lista, imprima uma mensagem para essa pessoa,
informando­o de que não pode convidá­lo para jantar.
• Imprima uma mensagem para cada uma das duas pessoas
que ainda estão na sua lista, informando­as de que
ainda estão convidadas.
• Use del para remover os dois últimos nomes da sua
lista, para que você tenha uma lista vazia. Imprima
sua lista para ter certeza de que você realmente tem
uma lista vazia no final do seu programa."""

def main():
	# Criando a lista de pessoas
	pessoas = createList()
	# Convidando as pessoas da lista
	inviteList(pessoas, ", convido-te para jantar.")
	# alguém não pode comparecer
	print((pessoas[0].title())+" não pode comparecer.\n Convide outra pessoa.")
	# removendo uma pessoa e adicionando outra
	editList(pessoas)
	# convidando pessoas que ainda estão na lista
	inviteList(pessoas, ", convido-te para jantar.")
	# convide mais pessoas por outros métodos
	print("Encontrei uma mesa maior, vou convidar mais pessoas!")
	# insira uma pessoa no início da lista, outra no meio e outra no final
	insertElement(pessoas)
	# convide quem está na lista
	inviteList(pessoas, ", convido-te para jantar.")
	# agora você infelizmente so pode convidar duas pessoas
	univiteList(pessoas, ",infelizmente não posso jantar contigo. Talvez na próxima, bye.")
	# convide as 2 pessoas restantes na lista
	inviteList(pessoas, ",convido-te para jantar.")
	# esvazie a lista
	delList(pessoas)

def delList(lista):
	for i in range(len(lista)):
		del lista[0]
		print(lista)

def univiteList(lista, msg):
	while True:
		# Menu para perguntar ação ao usuário
		action = input("\nDigite a tecla correspondente a ação desejada: \n "+
			"\t1 - Adicionar convidado a lista.\n"+
			"\t2 - Remover outro convidado.\n")
		# se for finalizar, entra no if
		if action == "1":
			print("\n\tVocê não pode adicionar mais convidados, sua mesa está lotada, já tem", len(lista), "convidados, e na sua mesa só cabem 2 !\n")
			continue
		if action == "2":
			uninvite = lista.pop()
			print("\n\t", uninvite, msg)
		if len(lista) == 2:
			print("\n\n")
			break
	return lista

def insertElement(lista):
	key = True
	while key == True:
		# Menu para perguntar ação ao usuário
		action = input("\nDigite a tecla correspondente a ação desejada: \n "+
			"\t1 - Adicionar convidado no início da lista.\n"+
			"\t2 - Adicionar convidado no meio da lista.\n"+
			"\t3 - Adicionar convidado no final da lista.\n"+
			"\t4 - Finalizar alterações.\n")
		# se for finalizar, entra no if
		if action == "1":
			lista.insert(0,(input("Nome do convidado a ser adicionado no início da lista: ")))
			continue
		if action == "2":
			lista.insert((int(((len(lista))+1)/2)),(input("Nome do convidado a ser adicionado no meio da lista: ")))
		if action == "3":
			lista.append(input("Nome do convidado a ser adicionado no final da lista: "))
			continue
		if action == "4":
			key = False
			continue
	return lista

def createList():
	# Criando uma lista vasia.
	lista = []
	# loop para o menu
	key = True
	while key == True:
		# Menu para perguntar ação ao usuário
		action = input("\nDigite a tecla correspondente a ação desejada: \n "+
			"\t1 - Para adicionar convidado.\n"+
			"\t2 - Para finalizar lista.\n")
		# se for finalizar, entra no if
		if action == "2":
			# torna a key falsa, finalizando o loop
			key = False
			continue
		# se for adicionar um novo elemento, entra no if
		if action == "1":
			# adicionando novo elemento ao final da lista
			lista.append(input("Informe o nome do convidado(a):  "))
			# retornando ao menu
			continue
	# retorna a lista completa
	return lista

def editList(lista):
	key = True
	while key == True:
		# Menu para perguntar ação ao usuário
		action = input("\nDigite a tecla correspondente a ação desejada: \n "+
			"\t1 - Para remover convidado.\n"+
			"\t2 - Para adicionar convidado.\n"+
			"\t3 - Para finalizar alterações.\n")
		# se for finalizar, entra no if
		if action == "1":
			lista.remove(input("Nome do convidado a ser removido: "))
			continue
		if action == "2":
			lista.append(input("Nome do convidado a ser adicionado: "))
			continue
		if action == "3":
			key = False
			continue
	return lista

# função que recebe uma lista e uma string a ser printada para cada elemento
def inviteList(lista, msg):
	for i in range(len(lista)):
		print(lista[i].title()+msg)
# executa o main
main()


	