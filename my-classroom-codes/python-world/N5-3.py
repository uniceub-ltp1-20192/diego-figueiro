""" Crie uma calculadora automática que dado dois valores
informe o resultado da adição, subtração, multiplicação e
divisão. """

num1=float(input("Digite o primeiro número:  "))
op = input("Insira a operação desejada: ")
num2 = float(input("Digite o segundo número: "))
if op== "+" :
    print(num1+num2)
elif op == "-":
    print(num1 - num2)
elif op == "*":
    print(num1*num2)
elif op == "/":
    print(num1 / num2)
else:
    print("Número/operação invalido(a)")
