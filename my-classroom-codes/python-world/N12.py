"""Um buffet oferece 5 tipos de comida. Pense em 5 tipos de
comida e as guarde em um tupla.
• Use um loop for para imprimir cada comida que o restaurante
oferece.
• Tente modificar um dos itens, e garanta que o python rejeita
a alteração.
• O restaurante mudou o menu, trocando dois pratos do cardápio.
Adicione um bloco de código que rescreve a tupla, e então
reusa o loop for para imprimir os itens do novo menu."""

def mainA():
	tupla = ('batatas', 'sorvete', 'coxinha', 'pizza', 'estrogonoff')
	for i in tupla:
		print(i)
def mainB():
	tupla[4] = "ratatouille"
def mainC():
	tupla = ('arroz', 'feijão', 'farofa', 'macarrão', 'estrogonoff')
	for i in tupla:
		print(i)
mainA()
mainC()
mainB()