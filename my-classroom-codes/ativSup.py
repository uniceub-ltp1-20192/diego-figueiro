# Faça um programa que receba uma quantidade de produtos, e o desconto a ser aplicado em cada produto, e assim, no final, printa o valor que pagaria, e printa o valor com desconto a ser pago.

qtd_produtos = int(input("Informe a quantidade de produtos que irão ser comprados: "))
totalSD = 0
totalCD = 0
vt = 0

for i in range(0, qtd_produtos):
	vp = int(input("Qual o valor do produto? "))
	totalSD = totalSD + vp
	vd = int(input("Qual o desconto a ser aplicado no produto? "))	
	vt = vp - (vp*(vd/100))
	totalCD = totalCD + vt

print("Você pagaria   : ", totalSD, "R$.")
print("Vai pagar      : ", totalCD, "R$.")
