from entities.cachorro import Cachorro

v = Cachorro()

v.tamanho = 3.4
v.peso = 3.9
v.cor = "Azul"

print("""
	O tamanho do cachorro é {}
	O peso do cachorro é {}
	A cor do cachorro é {}
	O latido do cachorro {}
	""".format(v.tamanho, v.peso, v.cor, v.latirComRetorno()))

print(vars(v))
