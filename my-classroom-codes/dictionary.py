''' Faça um set e experimente colocar dados dentro dele '''

meuSet = {"apple", "banana", "cherry"}
print(""" Meu primeiro Set:
	""", meuSet)

'''
Dictionary
'''
print("\n\tCriando um dicionário: \n")
dictionary = {
	"nome": "Diego",
	"idade": "18"
}
print(dictionary)
	#modificando a variável de uma chave já existente:
dictionary["nome"] = input("\nNome: ")
	#criando uma nova chave e solicitando uma variável:
dictionary["sexo"] = input("\nSexo: ")
print(dictionary)
	#solicitando uma nova chave a ser criada e solicitando sua variável:
chave = input("\nNome da chave: ")
print(chave, ": ")
dictionary[chave] = input("\t")
print(dictionary)
	#solicitando quantidade de chaves a serem adicionadas
key = int(input("\nQuantas chaves a serem adicionadas? "))
	#criando for com o range informado
for i in range(key):
	print("\nChave número ", i)
	chave = input("\nNome da chave: ")
	print(chave, ": ")
	dictionary[chave] = input("\t")
	print(dictionary)
