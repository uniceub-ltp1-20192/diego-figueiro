from random import shuffle


def main():
	# Começa o jogo 
	gameStart()


def generateCardDeck():
	SUITS          = ("SPADES", "HEARTS", "DIAMONDS", "CLUBS")

	cards = ["JOKER", ("DIAMONDS", 7), ("SPADES", 1), ("HEARTS", 7), ("CLUBS", 4)]

	for suit in SUITS:
		for value in range(11, 14):
			card = (suit, value)
			cards.append(card)
		for value in range(1, 4):
			card = (suit, value)
			cards.append(card)

	return cards

def generateShuffleDeck():
	deck = generateCardDeck()
	shuffle(deck)
	return deck

def dealCards(deck):
	p1 = deck[0:3]
	p2 = deck[4:7]
	p3 = deck[7:10]
	p4 = deck[10:13]
	return p1, p2, p3, p4

def gameScore(t1, t2):
		print("\n\t( Time ímpar ->", t1, "-", t2, "<- Time par )\n")

def choseCard(p1, p2, p3, p4):
	print("\nMão do P1 :", p1, 
		"\nMão do P2 :", p2, 
		"\nMão do P3 :", p3, 
		"\nMão do P4 :", p4)

	c1, c2, c3, c4 = cardValue(p1[int(input("P1 - Escolha a carta: 0, 1 ou 2: "))]), cardValue(p2[int(input("P2 - Escolha a carta: 0, 1 ou 2: "))]), cardValue(p3[int(input("P3 - Escolha a carta: 0, 1 ou 2: "))]), cardValue(p4[int(input("P4 - Escolha a carta: 0, 1 ou 2: "))])

	team1_3 = highestCard(c1, c3)
	team2_4 = highestCard(c2, c4)
	return team1_3, team2_4

def compareCards(t1, t2):
	if t1 < t2:
		print("Vitória do time 1\n")
		return "team1_3"
	
	if t2 > t1:
		print("Vitória do time 2\n")
		return "team2_4"
	
	print("Empate\n")
	return "EMPATE" # empate

def trucoRound():
	# Gereando e embaralhando as cartas
	deck = generateShuffleDeck()
	# Ditribuindo as cartas para os quatro jogadores
	p1, p2, p3, p4 = dealCards(deck)
	# Cada jogador escolherá sua carta
	for i in range(3):
		hand = compareCards(choseCard(p1, p2, p3, p4))

		if hand == "team1_3":
			team1_3 += 1
		elif hand == "team2_4":
			team2_4 +=1
	if team1_3 >= 2:
		return "team1_3"
	if team2_4 >= 2:
		return "team2_4"
	return "EMPATE"

def gameEnd(t1, t2):
	if t1 > t2:
		print("Vitória do Time Ímpar!!!")
	else:
		print("Vitória do Time Par!!!")
	gameScore(t1, t2)

def gameStart():
	team1_3, team2_4 = 0, 0
	matchPoint = 12

	while team1_3 < matchPoint and team2_4 < matchPoint:
		gameScore(team1_3, team2_4)
		point = trucoRound()

		if point == "team1_3":
			team1_3 += 1
		elif point == "team2_4":
			team2_4 += 1
		else:
			print("EMPATE")
	gameEnd(team1_3, team2_4)

def cardValue(card):
	manilhas = (("CLUBS", 4), ("HEARTS", 7), ("SPADES", 1), ("DIAMONDS", 7), "JOKER",)
	valueOrder = (12 , 11, 13, 1, 2, 3)

	if card in manilhas:
		return manilhas.index(card)

	return valueOrder.index(card[1]) + 5

def highestCard(card1, card2):
	if card1 < card2:
		return card1
	return card2

main()
