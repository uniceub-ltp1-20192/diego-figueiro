// No hotel morte lenta, estavam hospedados 5 hospédes, Renato Faca, Maria Cálice, Roberto Arma, Roberta Corda e Uesllei.
// A meia noite, o hóspede do quarto 101 foi encontrado morto e os 5 suspeitos são os citados acima.
// Descubra quem é o assassino sabendo que:
//        Se o nome do suspeito contem 3 vogais ou mais(Nome completo) e nenhuma dessas vogais é u ou o, ele pode ser o assassino.
//        Se o suspeito for do sexo feminino, só sera assassina se tiver utilizado uma arma branca.
//        Se o suspeito for do sexo masculino, ele deveria estar no saguão do hotel a 00h30 para ser considerado o assasino.
// Sabendo dessas informações acima, projete o algorítmo que fornecido o nome do suspeito, indique se ele pode ou não ser o assassino.

package main

import "fmt"
import "unicode"
import "strings"

func main() {

	// solcitar nome ao usuário
	name := askName()

	// verifica se o nome é uma string
	vn := validateString(name)
	if !vn {
		fmt.Println("Texto inserido não está na formatação de um nome.")
		return
	}

	vwl := validateVowel(name)
	if !vwl {
		fmt.Println("Inocente.")
		return
	}

	sex := checkSex(name)

	gun := getWeapon(name)

	fww := femaleww(sex, gun)

	mnm := midnightman(sex, time)

	if !(fww || mnm) {
		fmt.Println("Inocente.")
		return
	}
	fmt.Println("Possível assassino(a).")

	}
}

func askName() string {

	// declara a var input
	var name string

	// apresenta a string Nome on-the-face
	fmt.Print("Nome: ")
    
    // lê o que o usuário digitou
    fmt.Scanln(&name)

    return name
}

func IsLetter(s string) bool {
    for _, r := range s {
        if !unicode.IsLetter(r) {
            return false
        }
    }
    return true

func validateString(s string) bool {
	nospaces := name = strings.Replace(name, " ", "",-1)

	return IsLetter(nospaces)
}

func validateVowel(name string) bool {
	cntr := 0
	for vvowel, r := range name {
		if ((vvowel == 'o') || (vvowel == 'u')){
			return false
		}
		if ((vvowel == 'a') || (vvowel == 'e') || (vvowel == 'i')){
			cntr = cntr + 1
		}
		if cntr >= 3 {
			return true
		}
		return false
	}
	return
}

func checkSex(name string) bool {
	if ((name == "Maria Cálice")||(name== "Roberta Corda")){
		return true
	}
	return false
}

func getWeapon(name string) bool {
	result := strings.Split(name, " ")
	for getgun r:= range result {
		vg:= str(getgun)
		if (vg == "Arma"){
			return false
		}
	}
	return true
}

func femaleww(sex bool, gun bool) bool {
	if !(sex && gun){
		return false
	}
	return true
}

func midnightman(sex bool, time float) bool {
	if sex{
		return false
	}
	time := "sla"
	if (time == "00:30"){
		return true
	}
	return false

}


