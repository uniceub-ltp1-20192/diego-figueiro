class Human:
    def __init__(self, hashnumber, name, age, height):
        self._hash = hashnumber
        self._name = name
        self._age = age
        self._height = height

    @property
    def hash(self):
        return self._hash
    @hash.setter
    def hash(self, hashnumber):
        self._hash = hashnumber


    @property
    def name(self):
        return self._name
    @name.setter
    def name(self, name):
        self._name = name


    @property
    def age(self):
        return self._age
    @age.setter
    def age(self, age):
        self._age = age


    @property
    def height(self):
        return self._height
    @height.setter
    def height(self, height):
        self._height = height


    def readableTime(self, min):
        if min*364 != 0:
            dias = "{} dias".format(min*364)
        else:
            dias = ""

        return dias + " de vida, caso o ano tenha 364 dias. "

    def hasattribute(self, attribute):
        attributesList = [self._hash, self._name, str(self._age), self._height]
        
        if attribute in attributesList:
            return True
        return False

    def valuesToBePrinted(self):
        return """
        Hash   - {}
        Nome   - {}
        Idade  - {}
        Altura - {}\n""".format(self._hash[1:], self._name, self.readableTime(self.age), self._height)
        
    def printValues(self):
        print(self.valuesToBePrinted())

    def txtformat(self):
        return "{};{};{};{}".format(self._hash, self._name, self._age, self._height)




