from Entidades.human import Human

class Indian(Human):
    def __init__(self, hashnumber, name, age, height, gender, MathSkillz):
        super().__init__(hashnumber, name, age, height)
        self._gender         = gender
        self._MathSkillz = MathSkillz


    @property
    def gender(self):
        return self._gender
    @gender.setter
    def gender(self, gender):
        self._gender = gender


    @property
    def MathSkillz(self):
        return self._MathSkillz
    @MathSkillz.setter
    def MathSkillz(self, MathSkillz):
        self._MathSkillz = MathSkillz


    def hasAttribute(self, attribute):
        attributesList = [self._gender, self._MathSkillz]
        if super().hasAttribute(attribute) or attribute in attributesList:
            return True
        return False


    def valuesToBePrinted(self):
        return """\tNome do pai  - {}
        M.S.      - {}\n""".format(self._gender, self._MathSkillz)

    def printValues(self):
        print(super().valuesToBePrinted() + self.valuesToBePrinted())

    def txtformat(self):
        return super().txtformat() + ";{};{}".format(self._gender.name(), self._MathSkillz)