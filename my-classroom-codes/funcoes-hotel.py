#No hotel morte lenta, estavam hospedados 5 hospédes, Renato Faca, Maria Cálice, Roberto Arma, Roberta Corda e Uesllei.
#A meia noite, o hóspede do quarto 101 foi encontrado morto e os 5 suspeitos são os citados acima.
#Descubra quem é o assassino sabendo que:
#        Se o nome do suspeito contem 3 vogais ou mais(Nome completo) e nenhuma dessas vogais é u ou o, ele pode ser o assassino.
#        Se o suspeito for do sexo feminino, só sera assassina se tiver utilizado uma arma branca.
#        Se o suspeito for do sexo masculino, ele deveria estar no saguão do hotel a 00h30 para ser considerado o assasino.
# Sabendo dessas informações acima, projete o algorítmo que fornecido o nome do suspeito, indique se ele pode ou não ser o assassino.

import sys
import datetime
import string

time = str(datetime.datetime.now().time())

def askName():
	# Entra na função sem um parâmetro e retorna o nome.
	return input("Nome: ")

def validateString(name):
	# Determina se o texto digitado é valido. (contendo caracteres 'não especiais' e 'nenhum número')
	if name.replace(" ", "").isalpha():
		return True
	return False	

def validateVowel(name):
	# Se o nome do suspeito contem 3 vogais ou mais(Nome completo) e nenhuma dessas vogais é 'u' ou 'o', ele pode ser o assassino.
	charList = list(name)
	cntr = 0
	for vvowel in charList:
		if ((vvowel == 'o') or (vvowel == 'u')):
			return False	
		if ((vvowel == 'a') or (vvowel == 'e') or (vvowel == 'i')):
				cntr = cntr + 1
	if cntr >= 3:
		return True
	return False

def checkSex(name):
	# Checar o sexo referente ao nome. (True == mulher e False == homem)
	if ((name == "Maria Cálice") or (name == "Roberta Corda")):
		return True
	return False

def getWeapon(name):
	# Identificar o tipo de arma ultilizada de acordo com o sobre nome. (armas brancas ou não)
	lista = name.split()
	for getgun in lista:
		vg = str(getgun)
		if (vg == "Arma"):
			return False
	return True

def femaleww(sex, gun):
	# Se o suspeito for do sexo feminino, só sera assassina se tiver utilizado uma arma branca.
	if not(sex and gun):
		return False
	return True

def midnightman(sex, time):
	# Se o suspeito for do sexo masculino, ele deveria estar no saguão do hotel a 00h30 para ser considerado o assasino.

	if sex:
		return False
	time = str(datetime.datetime.now().time())
	if (time == "00:30:00.000000"):
		return True
	return False
	
# Pergunte o nome.
name = askName()

# Valide a formatação do nome.
vn = validateString(name)
	# Caso a formatação não seja válida:
if not vn:
	sys.exit("Texto inserido não está na formatação de um nome.")

# Verifique se o nome se enquadra na regra 1.
vwl = validateVowel(name)
	# Caso não se enquadre:
if not vwl:
	sys.exit("Inocente.")

# Consiga o gênero a partir do nome.
sex = checkSex(name)

# Consiga o tipo de arma ultilizada a partir do nome.
gun = getWeapon(name)

# Verifique se a suspeita se enquadra na regra 2. (sex == True & gun == True)
fww = femaleww(sex, gun)

# Verifique se o suspeito se enquadra na regra 3. (sex == False & time = "00:30:00.000000")
mnm = midnightman(sex, time)

# Caso o(a) suspeito(a) não se enquadre:
if not(fww or mnm):
	sys.exit("Inocente.")
	# Caso contrário, conclua:
print("Possível assassino(a).")	
