""" Atribua um valor a cebolas, depois informe quantas cebolas cabem na sua mochila, então, o programa responderá quantas faltam para encher ou quantas ficaram de fora"""

cebola = int(input("Informe o número de cebolas?"))
mochila = int(input("Quantas cebolas cabem na sua mochila?"))

if cebola < mochila:
    mochila -= cebola
    print("Faltam ", mochila, "cebolas para encher sua mochila")
elif cebola > mochila:
    cebola -= mochila
    print("Sobraram ", cebola, "cebolas que não couberam na sua mochila")
else:
    print("Sua mochila está cheia de cebolas")

